﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour
{
    [SerializeField] int soulCount = 3;
    public int SoulCount { get => soulCount; set => soulCount = value; }

    private void Start()
    {
        transform.localPosition = Vector3.zero;
    }
   
    private void OnCollisionEnter2D(Collision2D collision)
    {   
        ParentGrave(collision);
        ParentPlayer(collision);
        ParentEnemy(collision);
        ParentSoul(collision);
    }
    void ParentGrave(Collision2D collision)
    {
        if(transform.parent.gameObject.tag == "Grave")
        {            
            Grave g = transform.parent.gameObject.GetComponent<Grave>();

            if (g.AlreadyBorn == false)
            {
                if (collision.gameObject.tag == "Player")
                {                    
                    Inventory plrInv = LevelManager.INSTANCE.CurrentLevel.Player.GetInventory();
                    if (plrInv.SoulCount() >= soulCount)
                    {
                        List<Soul> plrSoulList = plrInv.GetSoulCount(soulCount);
                        g.GetInventory().AddSouls(plrSoulList);
                        AudioManager.INSTANCE.PlayOneShot(17);
                    }
                }
            }
        }
    }
    void ParentPlayer(Collision2D collision)
    {
        if(transform.parent.gameObject.tag == "Player")
        {
            Player p = transform.parent.gameObject.GetComponent<Player>();
            
            if (collision.gameObject.tag == "Soul")
            {                
                if (GameStateMashine.INSTANCE.TurnState == TurnState.day)
                {                    
                    Soul s = collision.gameObject.GetComponent<Soul>();
                    s.gameObject.SetActive(false);
                    p.GetInventory().AddSouls(s);
                    AudioManager.INSTANCE.PlayOneShot(16);
                }
            }
            if(collision.gameObject.tag == "Grave")
            {                
            }
            if(collision.gameObject.tag == "Enemy")
            {
                Enemy e = collision.gameObject.GetComponent<Enemy>();

                if(e.EnemyState == EnemyState.Live && e.EnemySide == EnemySide.Good && e.AlreadyHeal == false)
                {                   
                    p.HP += 5;
                    e.AlreadyHeal = true;
                    e.ShowBubble();
                    e.Stop();
                }
            }
        }
    }
    void ParentEnemy(Collision2D collision)
    {
        if (transform.parent.gameObject.tag == "Enemy")
        {
            Enemy e = transform.parent.gameObject.GetComponent<Enemy>();
            if(collision.gameObject.tag == "Player")
            {               
                Player p = LevelManager.INSTANCE.CurrentLevel.Player;
                if (GameStateMashine.INSTANCE.TurnState == TurnState.night)
                {
                    e.Shoot(p);
                    e.ShowBubble();
                }
            }            
        }
    }
    void ParentSoul(Collision2D collision)
    {
        if (transform.parent.gameObject.tag == "Soul")
        {
            Soul s = transform.parent.gameObject.GetComponent<Soul>();
            if (collision.gameObject.tag == "Player")
            {               
                Player p = collision.gameObject.GetComponent<Player>();
                if(GameStateMashine.INSTANCE.TurnState == TurnState.night)
                {
                    s.Boom(3);
                }

            }
            if (collision.gameObject.tag == "Enemy")
            {                
                Enemy e = collision.gameObject.GetComponent<Enemy>();
                if (GameStateMashine.INSTANCE.TurnState == TurnState.night)
                {
                    s.Boom(3);
                }
            }
        }
       
    }
}
