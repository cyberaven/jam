﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Player : ActorBase
{
    [SerializeField] SkeletonAnimation playerAnim;
    [SerializeField] Inventory inventory;
    Inventory _inventory;

    [SerializeField] Bullet bullet;
    Bullet _bullet;

    [SerializeField] Radar radar;
    Radar _radar;

    Level currentLevel;

    Coroutine coroutine;
    
    int _hp = 100;
    public int HP { get => _hp; set => _hp = value; }
    float deltaHP = 0f;

    Vector3 targetPoint;
    bool move;   
    [SerializeField] float keyboardMoveSpeed = 0.2f;

    bool death = false;

    public override void MyStart()
    {
        base.MyStart();
        _inventory = Instantiate(inventory, transform);
        _radar = Instantiate(radar, transform);
        currentLevel = LevelManager.INSTANCE.CurrentLevel;

        ChangeAnim("idle");

        Camera.main.transform.SetParent(transform);
        Camera.main.transform.localPosition = new Vector3(0,0,-10);
    }

    public override void MyUpdate()
    {
        base.MyUpdate();
        KeyboardMove();
        MousePointToMove();    
        MinusHpIfNight();
        CheckHP();
    }
   
    private void MousePointToMove()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Shoot();           
        }
    }
    void Shoot()
    {
        if (GameStateMashine.INSTANCE.TurnState == TurnState.night)
        {
            if (_inventory.SoulCount() > 0)
            {
                ChangeAnim("shoot");
                Vector3 mousePos = Input.mousePosition;
                targetPoint = Camera.main.ScreenToWorldPoint(mousePos);

                Bullet b = Instantiate(bullet);
                b.Parent = transform;
                b.transform.position = transform.position;
                b.SetDirection(targetPoint);
                _inventory.DelSoulCount(1);
                b = null;
            }
        }
    }
    public Inventory GetInventory()
    {
        return _inventory;
    }
    void MinusHpIfNight()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.night)
        {
            deltaHP = deltaHP + Time.deltaTime;
            if((int)deltaHP == 1)
            {
                HP -= 1;
                deltaHP = 0;
            }            
        }
        else
        {
            deltaHP = 0;
        }
    }
    void KeyboardMove()
    {
        if (death == false)
        {
            if (Input.GetKey("w"))
            {
                ChangeAnim("run");
                move = false;
                transform.position = new Vector3(transform.position.x, transform.position.y + keyboardMoveSpeed, 0);
            }
            if (Input.GetKey("s"))
            {
                ChangeAnim("run");
                move = false;
                transform.position = new Vector3(transform.position.x, transform.position.y - keyboardMoveSpeed, 0);
            }
            if (Input.GetKey("d"))
            {
                ChangeAnim("run");
                move = false;
                transform.position = new Vector3(transform.position.x + keyboardMoveSpeed, transform.position.y, 0);
            }
            if (Input.GetKey("a"))
            {
                ChangeAnim("run");
                move = false;
                transform.position = new Vector3(transform.position.x - keyboardMoveSpeed, transform.position.y, 0);
            }
            if (transform.position.x > currentLevel.LevelSizeX)
            {
                Vector3 newPos = new Vector3(transform.position.x - 1, transform.position.y, 0);
                transform.position = newPos;
            }
            if (transform.position.x < -currentLevel.LevelSizeX)
            {
                Vector3 newPos = new Vector3(transform.position.x + 1, transform.position.y, 0);
                transform.position = newPos;
            }
            if (transform.position.y > currentLevel.LevelSizeY)
            {
                Vector3 newPos = new Vector3(transform.position.x, transform.position.y - 1, 0);
                transform.position = newPos;
            }
            if (transform.position.y < -currentLevel.LevelSizeY)
            {
                Vector3 newPos = new Vector3(transform.position.x, transform.position.y + 1, 0);
                transform.position = newPos;
            }
        }
    }

    void ChangeAnim(string animName)
    {
        if(animName == "idle")
        {
            playerAnim.AnimationName = "HERO_IDLE";
        }
        if (animName == "run")
        {
            playerAnim.AnimationName = "HERO_RUN";
        }
        if(animName == "shoot")
        {
            playerAnim.AnimationName = "HERO_FIRE";
        }
        if(animName == "die")
        {
            playerAnim.AnimationName = "HERO_DEATH";
        }
    }
    void CheckHP()
    {
        if (death == false)
        {
            if (HP < 0)
            {
                Death();
            }
        }
    }
    void Death()
    {
        death = true;
        ChangeAnim("die");
        StartCoroutine("WaitSecond");
        GameStateMashine.INSTANCE.GameState = GameState.stop;
    }
    IEnumerator WaitSecond()
    {
        yield return new WaitForSeconds(2);
        StopCoroutine("WaitSecond");
        Destroy(LevelManager.INSTANCE.CurrentLevel.gameObject);
        Camera.main.transform.SetParent(null);
        GameUI.INSTANCE.ShowMainMenu();
    }

}
