﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    [SerializeField] List<Soul> soulsList;
    private void Start()
    {
        soulsList = new List<Soul>();
    }
    public void AddSouls(Soul soul)
    {
        soulsList.Add(soul);
    }
    public void AddSouls(List<Soul> souls)
    {
        foreach (Soul soul in souls)
        {
            soulsList.Add(soul);
        }
    }
    public int SoulCount()
    {            
        return soulsList.Count;       
    }
    public Soul GetSoul(int id)
    {
        Soul s = soulsList[id];
        soulsList[id] = null;
        RepackList();
        return s;
    }
    void RepackList()
    {
        List<Soul> s = new List<Soul>();
        for (int i = 0; i <= soulsList.Count-1; i++)
        {
            if (soulsList[i] != null)
            {
                s.Add(soulsList[i]);
            }
        }
        soulsList = s;        
    }
    public void DelSoulCount(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Destroy(soulsList[i].gameObject);
            soulsList[i] = null;
        }
        RepackList();
    }
    public List<Soul> GetSoulCount(int count)
    {
        List<Soul> s = new List<Soul>();
        for (int i = 0; i < count; i++)
        {
            s.Add(soulsList[i]);
            soulsList[i] = null;
        }
        RepackList();
        return s;
    }
}
