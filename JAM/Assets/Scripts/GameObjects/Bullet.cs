﻿using UnityEngine;
using System.Collections;

public class Bullet : ActorBase
{
    Vector3 direction;
    Transform parent;
    public Transform Parent { get => parent; set => parent = value; }

    float liveTime = 3;
    int moveSpeed = 100;
    int damage = 10;

    

    public override void MyStart()
    {
        base.MyStart();
    }
    public override void MyUpdate()
    {
        base.MyUpdate();
        LiveTimeTimer();
        FlyAway();
    }
    public override void MyOnCollisionEnter2D(Collision2D collision)
    {
        base.MyOnCollisionEnter2D(collision);
        if(parent.gameObject.tag == "Player")
        {
            if (collision.gameObject.tag == "Enemy")
            {
                Enemy e = collision.gameObject.GetComponent<Enemy>();
                Destroy(e.gameObject);
                Destroy(this.gameObject);
            }
        }
        if(parent.gameObject.tag == "Enemy")
        {
            if (collision.gameObject.tag == "Player")
            {
                Player p = LevelManager.INSTANCE.CurrentLevel.Player;
                p.HP -= damage;
                Destroy(this.gameObject);
            }
        }
        
    }
    void LiveTimeTimer()
    {
        liveTime -= Time.deltaTime;
        if(liveTime < 0)
        {
            Destroy(this.gameObject);
        }

    }
    void FlyAway()
    {
        Vector3 newPos = Vector3.MoveTowards(transform.localPosition, direction, moveSpeed * Time.deltaTime);
        newPos.z = 0;
        transform.position = newPos;
    }

    public void SetDirection(Vector3 dir)
    {
        direction = dir;
    }
}
