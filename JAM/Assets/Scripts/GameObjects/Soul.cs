﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Soul : MonoBehaviour
{
    [SerializeField] int damageToPlayer = 10;
    [SerializeField] Radar radar;
    Radar _radar;
    [SerializeField] TextMesh textMesh;

    [SerializeField] Sprite day;
    [SerializeField] Sprite night;

    [SerializeField] SpriteRenderer view;

    int timeToBoom;
    bool readyToBoom = false;

    [SerializeField] ParticleSystem boom;

    [SerializeField] SkeletonAnimation goodAnim;
    [SerializeField] SkeletonAnimation evilAnim;

    private void Start()
    {
        view = GetComponentInChildren<SpriteRenderer>();
        _radar = Instantiate(radar, transform);
        _radar.GetComponent<CircleCollider2D>().radius = 2;

        goodAnim.gameObject.SetActive(true);
        evilAnim.gameObject.SetActive(false);

        GameStateMashine.TurnStateChangedEvent += TurnStateChanged;
    }
    private void Update()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            view.sprite = null;
        }
        else
        {
            view.sprite = null;
        }
        if(readyToBoom == false)
        {
            textMesh.gameObject.SetActive(false);
        }
        else
        {
            textMesh.gameObject.SetActive(true);
            textMesh.text = timeToBoom.ToString();
        }
    }
    public void Boom(int sec)
    {
        if (readyToBoom == false)
        {            
            timeToBoom = sec;
            readyToBoom = true;
            StartCoroutine("BoomCourutine");
        }
    }
    IEnumerator BoomCourutine()
    {        
        bool a = true;
        ContactFilter2D conF = new ContactFilter2D();
        conF.NoFilter();

        while (a)
        {
            yield return new WaitForSeconds(1f);
            timeToBoom -= 1;
            if (timeToBoom == 0)
            {                
                List<Collider2D> col = new List<Collider2D>();
                _radar.GetComponent<CircleCollider2D>().OverlapCollider(conF, col);
                
                foreach(Collider2D c in col)
                {
                    Player p = c.GetComponent<Player>();
                    if (p == null)
                    {
                        if (c.gameObject.tag == "Enemy")
                        {
                            c.GetComponent<Enemy>().Death();                           
                        }
                    }
                    else
                    {                     
                        p.HP -= damageToPlayer;
                    }
                }
                col = null;               
                a = false;
                StopCoroutine("BoomCourutine");
                Death(); 
            }            
        }
    }
    void Death()
    {       
        Level l = LevelManager.INSTANCE.CurrentLevel;
        ParticleSystem p = Instantiate(boom, l.transform);
        p.transform.localPosition = transform.localPosition;

        GameStateMashine.TurnStateChangedEvent -= TurnStateChanged;
        Destroy(this.gameObject);
    }
    void TurnStateChanged()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            if (goodAnim != null && evilAnim != null)
            {
                goodAnim.gameObject.SetActive(true);
                evilAnim.gameObject.SetActive(false);
            }
        }
        else
        {
            if (goodAnim != null && evilAnim != null)
            {
                goodAnim.gameObject.SetActive(false);
                evilAnim.gameObject.SetActive(true);
            }
        }
    }
}
