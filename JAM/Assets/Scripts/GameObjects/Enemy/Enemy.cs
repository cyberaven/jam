﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum EnemySide
{
    Good,
    Evil
}
public enum EnemyState
{
    Stoped,
    Live,
    Dead,    
}

public class Enemy : ActorBase
{
    [SerializeField] SkeletonAnimation whiteAnim;
    [SerializeField] SkeletonAnimation zombyAnim;
    [SerializeField] SkeletonAnimation devilAnim;

    [SerializeField] Sprite good;
    [SerializeField] Sprite evil;
    [SerializeField] Sprite evilBornNight;
    [SerializeField] SpriteRenderer view;

    [SerializeField] Radar radar;
    Radar _radar;
    [SerializeField] Bullet bullet;
    Bullet _bullet;

    [SerializeField] EnemySide _enemySide = EnemySide.Good;
    public EnemySide EnemySide { get => _enemySide; set => _enemySide = value; }

    [SerializeField] EnemyState _enemyState = EnemyState.Live;
    public EnemyState EnemyState { get => _enemyState; set => _enemyState = value; }
    
    [SerializeField] Bubble bubble;
    Bubble _bubble;
    int bubbleShowTime = 160;
    int _bubbleShowTime = 160;
    int bubbleHideTime = 160;    
    int _bubbleHideTime = 160;    

    Vector3 targetPoint;
    bool move = false;
    float speed = 2;

    bool alreadyHeal = false;
    public bool AlreadyHeal { get => alreadyHeal; set => alreadyHeal = value; }

    [SerializeField] bool bornNight = false;
    public bool BornNight { get => bornNight; set => bornNight = value; }
    
    public override void MyStart()
    {
        base.MyStart();
        GameStateMashine.TurnStateChangedEvent += TurnStateChanged;
        Init();
    }

    public override void MyUpdate()
    {
        base.MyUpdate();
        MoveToPoint();
        ChangeMovePoint();
        MoveBubbleOverPlayer();
        BubbleFreqOfDispaly();           
    }
    void Init()
    {
        _radar = Instantiate(radar, transform);
        _radar.GetComponent<CircleCollider2D>().radius = 2;
        _bubble = Instantiate(bubble, transform);
        ChangeBornNight();
        ChangeMySide();
        ChangeMyColor();        
        HideBubble();               
        ChangeMovePoint();
        ChangeAnim();
    }
    void ChangeMovePoint()
    {
        if (_enemyState == EnemyState.Live)
        {
            if (move == false)
            {
                if (_enemySide == EnemySide.Good)
                {
                    Level l = LevelManager.INSTANCE.CurrentLevel;
                    int levelSizeX = l.LevelSizeX;
                    int levelSizeY = l.LevelSizeY;
                    targetPoint = new Vector3(Random.Range(-levelSizeX, levelSizeX), Random.Range(-levelSizeY, levelSizeY), 0);
                    move = true;
                }
                if (_enemySide == EnemySide.Evil)
                {
                    Player p = LevelManager.INSTANCE.CurrentLevel.Player;
                    targetPoint = p.transform.position;
                    move = true;                   
                }
            }
        }
    }
    private void MoveToPoint()
    {
        if (_enemyState == EnemyState.Live)
        {
            if (move == true)
            {                
                Vector3 newPos = Vector3.MoveTowards(transform.position, targetPoint, speed * Time.deltaTime);
                newPos.z = 0;
                transform.position = newPos;             

                if (transform.position == targetPoint || _enemySide == EnemySide.Evil)
                {
                    move = false;
                }
            }
        }
    }  
   
    void MoveBubbleOverPlayer()
    {
        _bubble.transform.localPosition = Vector3.zero;
    }
    void HideBubble()
    {
        _bubble.gameObject.SetActive(false);
    }
    public void ShowBubble()
    {
        _bubble.gameObject.SetActive(true);
        AudioManager.INSTANCE.PlayRandTrueFact();
    }
    void BubbleFreqOfDispaly()
    {        
        if(_bubble.gameObject.activeSelf == true)
        {
            _bubbleShowTime -= 1;
            if(_bubbleShowTime == 0)
            {
                _bubbleShowTime = bubbleShowTime;
                HideBubble();
            }

        }
    }
    public void Stop()
    {
        _enemyState = EnemyState.Stoped;
    }
    void TurnStateChanged()
    {      
        if (GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            if (bornNight == true)
            {                
                Death();                                
            }
            else
            {
                _enemyState = EnemyState.Live;
                move = false;                
            }
        }
        else
        {
            _enemyState = EnemyState.Live;           
            move = false;
        }
        ChangeMySide();
        ChangeMyColor();
        ChangeAnim();
    }
    void ChangeMySide()
    {
        if (GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {           
            _enemySide = EnemySide.Good;           
        }
        else
        {            
            _enemySide = EnemySide.Evil;           
        }
    }
    void ChangeMyColor()
    {       
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            view.sprite = null;
        }
        else
        {
            if(bornNight == true)
            {
                view.sprite = null;
            }
            else
            {
                view.sprite = null;
            }
        }
    }
    public void Shoot(Player p)
    {
        Player player = LevelManager.INSTANCE.CurrentLevel.Player;
        Bullet b = Instantiate(bullet,transform);        
        b.Parent = transform;
        b.transform.localPosition = transform.localPosition;
        b.SetDirection(player.transform.localPosition);
        b = null;

        //Bullet b = Instantiate(bullet);
        //b.Parent = transform;
        //b.transform.position = transform.position;
        //b.SetDirection(targetPoint);
        //_inventory.DelSoulCount(1);
        //b = null;
    }    
    void ChangeBornNight()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            bornNight = false;
        }
        else
        {
            bornNight = true;
        }
    }
    public void Death()
    {
        GameStateMashine.TurnStateChangedEvent -= TurnStateChanged;
        Destroy(this.gameObject);
    }
    void ChangeAnim()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            whiteAnim.gameObject.SetActive(true);
            zombyAnim.gameObject.SetActive(false);
            devilAnim.gameObject.SetActive(false);
        }
        else
        {
            if(bornNight == true)
            {
                whiteAnim.gameObject.SetActive(false);
                zombyAnim.gameObject.SetActive(true);
                devilAnim.gameObject.SetActive(false);
            }
            else
            {
                whiteAnim.gameObject.SetActive(false);
                zombyAnim.gameObject.SetActive(false);
                devilAnim.gameObject.SetActive(true);
            }
        }
    }
    private void OnDestroy()
    {
        GameStateMashine.TurnStateChangedEvent -= TurnStateChanged;
    }
}
