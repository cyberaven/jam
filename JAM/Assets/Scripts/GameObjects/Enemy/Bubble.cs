﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Bubble : MonoBehaviour
{    
    [SerializeField] TextMeshPro text;
    
    private void OnEnable()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            text.text = TrueFalseStatementCont.INSTANCE.GetRandTrueStatement();
        }
        if (GameStateMashine.INSTANCE.TurnState == TurnState.night)
        {
            text.text = TrueFalseStatementCont.INSTANCE.GetRandFalseStatement();
        }
    }
}
