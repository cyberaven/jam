﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grave : ActorBase
{
    [SerializeField] List<Sprite> sprites;
    [SerializeField] Enemy enemy;
    [SerializeField] Radar radar;
    Radar _radar;
    [SerializeField] float radarSize = 2;
    [SerializeField] int soulForRise = 3;
    [SerializeField] Inventory inventory;
    Inventory _inventory;

    [SerializeField] bool alreadyBorn = false;

    public bool AlreadyBorn { get => alreadyBorn; set => alreadyBorn = value; }

    public override void MyStart()
    {
        base.MyStart();
        _inventory = Instantiate(inventory, transform);
  
        _radar = Instantiate(radar, transform);
        _radar.GetComponent<CircleCollider2D>().radius = radarSize;
        _radar.SoulCount = soulForRise;
        GetComponentInChildren<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Count)];
        GameStateMashine.TurnStateChangedEvent += TurnStateChanged;
    }
    public override void MyUpdate()
    {
        base.MyUpdate();
        if(_inventory.SoulCount() >= 3)
        {
            BornEnemyCount(1);
            _inventory.DelSoulCount(3);
        }
    }
    void BornEnemyCount(int count)
    {
        Level l = LevelManager.INSTANCE.CurrentLevel;        

        for (int i = 0; i <= count-1; i++)
        {
            Enemy e = Instantiate(enemy, l.transform);
            e.transform.localPosition = transform.localPosition;
        }
        alreadyBorn = true;
    }
    public Inventory GetInventory()
    {
        return _inventory;
    }
    void TurnStateChanged()
    {
        if (GameStateMashine.INSTANCE.TurnState == TurnState.night)
        {
            if (alreadyBorn != true)
            {
                AudioManager.INSTANCE.PlayOneShot(18);
                BornEnemyCount(1);
            }
        }
        else
        {
            alreadyBorn = false;
        }
    }
    private void OnDestroy()
    {
        GameStateMashine.TurnStateChangedEvent -= TurnStateChanged;
    }
}
