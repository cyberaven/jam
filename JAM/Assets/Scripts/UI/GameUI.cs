﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public static GameUI INSTANCE;

    [SerializeField] MainMenu mainMenu;
    [SerializeField] LevelUI levelUI;

    void Start()
    {
        INSTANCE = this;

        GetComponent<Canvas>().worldCamera = Camera.main;

        mainMenu = Instantiate(mainMenu,transform);
        levelUI = Instantiate(levelUI,transform);

        GetComponent<Canvas>().sortingLayerName = "6";

        HideAll();
        ShowMainMenu();
    }

    private void Update()
    {
        if(GameStateMashine.INSTANCE.GameState == GameState.play)
        {
            if (levelUI.gameObject.activeSelf == false)
            {
                ShowLevelUI();
            }
        }
    }

    public void ShowMainMenu()
    {
        HideAll();
        mainMenu.Show();
    }
    void HideMainMenu()
    {
        mainMenu.Hide();
    }
    void ShowLevelUI()
    {
        HideAll();
        levelUI.Show();
    }
    void HideLevelUI()
    {
        levelUI.Hide();
    }
    void HideAll()
    {
        HideMainMenu();
        HideLevelUI();
    }

}
