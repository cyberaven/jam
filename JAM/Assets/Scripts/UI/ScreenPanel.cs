﻿using UnityEngine;
using System.Collections;

public class ScreenPanel : MonoBehaviour
{
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        MyUpdate();
    }
    public virtual void MyUpdate() { }
}
