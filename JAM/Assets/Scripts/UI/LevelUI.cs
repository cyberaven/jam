﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelUI : MonoBehaviour
{
    [SerializeField] Text timeTxt;
    [SerializeField] Image sun;
    [SerializeField] Text soulsCountTxt;
    [SerializeField] Text hpTxt;
    [SerializeField] Image darkness;
    [SerializeField] Sprite sunSpr;
    [SerializeField] Sprite moonSpr;
          
    private void Update()
    {   
        if (GameStateMashine.INSTANCE.GameState == GameState.play)
        {           
            ChangeSunColor();
            SetTime(GameTime.INSTANCE.TimeStr);
            SetSoulsCount();
            SetHpText();
            ShowDarkness();
            ChangeTextColor();
        }
    }       

    public void SetTime(string time)
    {
        timeTxt.text = time;
    }    
    void ChangeSunColor()
    {       
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            sun.sprite = sunSpr;
        }
        else
        {
            sun.sprite = moonSpr;
        }
    }
    void SetSoulsCount()
    {        
        int soulCount = LevelManager.INSTANCE.CurrentLevel.Player.GetInventory().SoulCount();        
        soulsCountTxt.text = "Souls: " + soulCount;
    }
    void SetHpText()
    {
        int hp = LevelManager.INSTANCE.CurrentLevel.Player.HP;
        if(hp < 30)
        {
            hpTxt.color = Color.red;
        }
        else
        {
            hpTxt.color = Color.white;
        }
        hpTxt.text = "HP: " + hp;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    void ShowDarkness()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.night)
        {
            darkness.gameObject.SetActive(true);
        }
        else
        {
            darkness.gameObject.SetActive(false);
        }
    }
    void ChangeTextColor()
    {
        if (GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            timeTxt.color = Color.black;
            soulsCountTxt.color = Color.black;
            hpTxt.color = Color.black;
        }
        else
        {
            timeTxt.color = Color.white;
            soulsCountTxt.color = Color.white;
            hpTxt.color = Color.white;
        }
    }    
}
