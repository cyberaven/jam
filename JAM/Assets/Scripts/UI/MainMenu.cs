﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : ScreenPanel
{
    [SerializeField] Button startBtn;
    [SerializeField] Button exitBtn;

    private void Start()
    {
        startBtn.onClick.AddListener(StartBtnClk);
        exitBtn.onClick.AddListener(ExitBtnClk);
    }

    void StartBtnClk()
    {
        LevelManager.INSTANCE.CreateLevel(0);
        GameStateMashine.INSTANCE.GameState = GameState.play;
        
    }
    void ExitBtnClk()
    {
        Application.Quit();

    }
}
