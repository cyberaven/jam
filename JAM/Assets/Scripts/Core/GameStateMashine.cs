﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    play,
    stop,
    pause
}
public enum TurnState
{
    night,
    day
}

public class GameStateMashine : MonoBehaviour
{
    public static GameStateMashine INSTANCE;

    [SerializeField] GameState gameState;
    public GameState GameState
    {
        get
        {
            return gameState;
        }
        set
        {
            gameState = value;
            if(GameStateChangedEvent != null)
            {
                GameStateChangedEvent();
            }
        }
    }
    
    [SerializeField] TurnState turnState;
    public TurnState TurnState
    {
        get
        {
            return turnState;
        }
        set
        {
            turnState = value;
            if(TurnStateChangedEvent != null)
            {
                TurnStateChangedEvent();
            }
        }
    }

    public delegate void GameStateChangedDel();
    public static event GameStateChangedDel GameStateChangedEvent;

    public delegate void TurnStateChangedDel();
    public static TurnStateChangedDel TurnStateChangedEvent;

    private void Start()
    {
        Init();
    }
    private void Init()
    {
        INSTANCE = this;

        gameState = GameState.stop;
        turnState = TurnState.day;
    }
    public void ChangeTurnState()
    {
        turnState = (turnState == TurnState.day) ? TurnState.night : TurnState.day;
    }
}
