﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level : MonoBehaviour
{
    [SerializeField] Player player;    
    Player _player;
    public Player Player { get => _player; set => _player = value; }

    [SerializeField] int levelSizeX = 50;
    public int LevelSizeX { get => levelSizeX; }
    [SerializeField] int levelSizeY = 50;
    public int LevelSizeY { get => levelSizeY; }
   
    [SerializeField] Soul soul;
    [SerializeField] int soulsCount = 20;
    [SerializeField] List<Soul> souls;

    [SerializeField] Grave grave;
    [SerializeField] int gravesCount = 10;
    [SerializeField] List<Grave> graves; 

    private void Start()
    {
        Init();       
    }
    void Init()
    {
        GameStateMashine.TurnStateChangedEvent += TurnStateChanged;
        souls = new List<Soul>();
        graves = new List<Grave>();
        StartLevel();       
        CreatePlayer();       
    }
    
    void StartLevel()
    {
        GameStateMashine.INSTANCE.GameState = GameState.play;       
        GameStateMashine.INSTANCE.TurnState = TurnState.day;       
    }    
    void CreatePlayer()
    {
        Player = Instantiate(player,transform);
    }
    void CreateGrave(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Grave g = Instantiate(grave, transform);
            g.transform.localPosition = new Vector3(Random.Range(-levelSizeX, levelSizeX), Random.Range(-levelSizeY, levelSizeY), 10);
            graves.Add(g);
        }        
    }   
    void CreateSouls(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Soul s = Instantiate(soul, transform);
            s.transform.localPosition = new Vector3(Random.Range(-levelSizeX, levelSizeX), Random.Range(-levelSizeY, levelSizeY), 10);
            souls.Add(s);
        }
    }     
    void TurnStateChanged()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            RepackGraveList();
            RepackSoulList();
            int sC = soulsCount - souls.Count;
            CreateSouls(sC);
            int gC = gravesCount - graves.Count;
            CreateGrave(gC);            
        }
    }
    void RepackSoulList()
    {
        List<Soul> s = new List<Soul>();
        for (int i = 0; i <= souls.Count - 1; i++)
        {
            if (souls[i] != null)
            {
                s.Add(souls[i]);
            }
        }
        souls = s;
    }
    void RepackGraveList()
    {
        List<Grave> g = new List<Grave>();
        for (int i = 0; i <= graves.Count - 1; i++)
        {
            if (graves[i] != null)
            {
                g.Add(graves[i]);
            }
        }
        graves = g;
    }
    private void OnDestroy()
    {
        GameStateMashine.TurnStateChangedEvent -= TurnStateChanged;
    }

}
