﻿using UnityEngine;
using System.Collections;

public class GameTime : MonoBehaviour
{
    public static GameTime INSTANCE;

    float time;
    [SerializeField] float turnTime = 60;
    public float Time { get => time; }

    string timeStr;
    public string TimeStr { get => timeStr; }

    private void Start()
    {
        INSTANCE = this;
        time = turnTime;
    }

    private void Update()
    {
        if(GameStateMashine.INSTANCE.GameState == GameState.play)
        {
            TimeGo();
        }
    }

    void TimeGo()
    {
        time -= UnityEngine.Time.deltaTime;
        int timeX = (int)time;
        timeStr = timeX.ToString();
        if(timeX == 0)
        {
            time = turnTime;
            ChangeTurnState();
        }        
    }
    void ChangeTurnState()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            GameStateMashine.INSTANCE.TurnState = TurnState.night;
        }
        else
        {
            GameStateMashine.INSTANCE.TurnState = TurnState.day;
        }
    }

}
