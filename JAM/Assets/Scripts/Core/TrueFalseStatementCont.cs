﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrueFalseStatementCont : MonoBehaviour
{
    public static TrueFalseStatementCont INSTANCE;
    [SerializeField] List<string> trueStatement;
    [SerializeField] List<string> falseStatement;

    private void Start()
    {
        INSTANCE = this;
    }

    public string GetRandTrueStatement()
    {
        return trueStatement[Random.Range(0, trueStatement.Count)];
    }
    public string GetRandFalseStatement()
    {
        return falseStatement[Random.Range(0, trueStatement.Count)];
    }
}
