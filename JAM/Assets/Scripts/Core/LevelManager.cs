﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    public static LevelManager INSTANCE;

    [SerializeField] List<Level> levels;

    Level currentLevel;

    public Level CurrentLevel { get => currentLevel; set => currentLevel = value; }

    private void Start()
    {
        INSTANCE = this;       
    }
        
    public void CreateLevel(int id)
    {
        currentLevel = null;
        currentLevel = Instantiate(levels[id]);
    }
}
