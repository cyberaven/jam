﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] GameStateMashine gameStateMashine;
    [SerializeField] GameTime gameTime;
    [SerializeField] GameUI gameUI;
    [SerializeField] TrueFalseStatementCont trueFalseStatementCont;
    [SerializeField] LevelManager levelManager;
    [SerializeField] AudioManager audioManager;
    
    void Start()
    {
        Instantiate(gameStateMashine, transform);
        Instantiate(gameTime, transform);
        Instantiate(gameUI);
        Instantiate(trueFalseStatementCont, transform);
        Instantiate(audioManager, transform);

        Instantiate(levelManager, transform);
        
    }

}
