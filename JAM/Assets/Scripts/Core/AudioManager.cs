﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager INSTANCE;
    [SerializeField] List<AudioClip> audioClips;
    AudioSource audio;



    private void Start()
    {
        INSTANCE = this;
        audio = GetComponent<AudioSource>();
        GameStateMashine.TurnStateChangedEvent += TurnStateChanged;
    }
    public void PlayOneShot(int id)
    {
        audio.PlayOneShot(audioClips[id]);
    }
    public void PlayRandTrueFact()
    {
        Debug.Log("PlayRandTrueFact");
        int id = Random.Range(7, 10);
        PlayOneShot(id);
    }
    public void PlayLevelMusicLoop(int id)
    {
        audio.clip = audioClips[id];
        audio.loop = true;
        audio.Play(0);
    }
    public void StopLevelMusic()
    {
        audio.Stop();
    }
    void TurnStateChanged()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            PlayOneShot(15);
            int r = Random.Range(0,3);
            PlayLevelMusicLoop(r);
        }
        else
        {
            PlayOneShot(14);
            int r = Random.Range(3, 7);
            PlayLevelMusicLoop(r);
        }
    }


}
