﻿using UnityEngine;
using UnityEditor;

public class ActorBase : MonoBehaviour
{
    private void Start()
    {
        MyStart();
    }
    private void Update()
    {
        MyUpdate();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        MyOnCollisionEnter2D(collision);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MyOnTriggerEnter2D(collision);
    }



    public virtual void MyStart(){}
    public virtual void MyUpdate(){}
    public virtual void MyOnTriggerEnter2D(Collider2D collision){}
    public virtual void MyOnCollisionEnter2D(Collision2D collision){}
}