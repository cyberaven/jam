﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGForLevel : MonoBehaviour
{
    [SerializeField] Sprite day;
    [SerializeField] Sprite night;
    [SerializeField] SpriteRenderer view;

    private void Start()
    {
        view = GetComponentInChildren<SpriteRenderer>();
        GameStateMashine.TurnStateChangedEvent += TurnStateChanged;
    }

    void TurnStateChanged()
    {
        if(GameStateMashine.INSTANCE.TurnState == TurnState.day)
        {
            view.sprite = day;
        }
        else
        {
            view.sprite = night;
        }
    }
    private void OnDestroy()
    {
        GameStateMashine.TurnStateChangedEvent -= TurnStateChanged;
    }
}
