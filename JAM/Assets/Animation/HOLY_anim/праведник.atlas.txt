
праведник.png
size: 2030,857
format: RGBA8888
filter: Linear,Linear
repeat: none
HOLY_ARM_LEFT
  rotate: true
  xy: 2, 64
  size: 337, 680
  orig: 337, 680
  offset: 0, 0
  index: -1
HOLY_ARM_RIGHT
  rotate: true
  xy: 1351, 15
  size: 307, 677
  orig: 307, 677
  offset: 0, 0
  index: -1
HOLY_BODY
  rotate: true
  xy: 2, 403
  size: 452, 1354
  orig: 452, 1354
  offset: 0, 0
  index: -1
HOLY_BORODA
  rotate: false
  xy: 1226, 199
  size: 123, 202
  orig: 123, 202
  offset: 0, 0
  index: -1
HOLY_BROVI
  rotate: false
  xy: 985, 2
  size: 184, 66
  orig: 184, 66
  offset: 0, 0
  index: -1
HOLY_EYE
  rotate: true
  xy: 1226, 26
  size: 171, 94
  orig: 171, 94
  offset: 0, 0
  index: -1
HOLY_HEAD
  rotate: false
  xy: 684, 6
  size: 299, 395
  orig: 299, 395
  offset: 0, 0
  index: -1
HOLY_HEARS
  rotate: true
  xy: 1358, 324
  size: 531, 614
  orig: 531, 614
  offset: 0, 0
  index: -1
HOLY_LEG_LEFT
  rotate: true
  xy: 985, 228
  size: 173, 239
  orig: 174, 239
  offset: 1, 0
  index: -1
HOLY_LEG_RIGHT
  rotate: true
  xy: 985, 70
  size: 156, 239
  orig: 156, 239
  offset: 0, 0
  index: -1
HOLY_MOUTH
  rotate: true
  xy: 1974, 793
  size: 62, 45
  orig: 62, 45
  offset: 0, 0
  index: -1
HOLY_MOUTH2
  rotate: true
  xy: 2, 2
  size: 60, 65
  orig: 60, 65
  offset: 0, 0
  index: -1
