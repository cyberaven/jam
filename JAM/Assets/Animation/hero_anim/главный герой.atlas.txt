
главный герой.png
size: 1845,764
format: RGBA8888
filter: Linear,Linear
repeat: none
HERO_ARM_LEFT
  rotate: true
  xy: 843, 419
  size: 101, 727
  orig: 101, 727
  offset: 0, 0
  index: -1
HERO_ARM_RIGHT
  rotate: true
  xy: 2, 48
  size: 117, 787
  orig: 117, 788
  offset: 0, 1
  index: -1
HERO_BODY
  rotate: true
  xy: 2, 396
  size: 366, 839
  orig: 366, 844
  offset: 0, 5
  index: -1
HERO_EYE
  rotate: true
  xy: 1762, 607
  size: 155, 73
  orig: 155, 73
  offset: 0, 0
  index: -1
HERO_HEAD
  rotate: false
  xy: 843, 36
  size: 452, 381
  orig: 452, 381
  offset: 0, 0
  index: -1
HERO_HELM
  rotate: false
  xy: 1487, 58
  size: 278, 292
  orig: 278, 292
  offset: 0, 0
  index: -1
HERO_LEG_LEFT
  rotate: true
  xy: 843, 522
  size: 240, 727
  orig: 240, 733
  offset: 0, 0
  index: -1
HERO_LEG_RIGHT
  rotate: true
  xy: 2, 167
  size: 227, 783
  orig: 227, 786
  offset: 0, 0
  index: -1
HERO_MOUTH
  rotate: true
  xy: 1824, 555
  size: 50, 19
  orig: 50, 19
  offset: 0, 0
  index: -1
HERO_MOUTH_2
  rotate: false
  xy: 2, 2
  size: 113, 44
  orig: 113, 44
  offset: 0, 0
  index: -1
HERO_MOUTH_3
  rotate: true
  xy: 1762, 484
  size: 121, 60
  orig: 121, 60
  offset: 0, 0
  index: -1
HERO_USIKI
  rotate: true
  xy: 787, 306
  size: 88, 27
  orig: 88, 27
  offset: 0, 0
  index: -1
LAMP_off
  rotate: false
  xy: 1572, 352
  size: 188, 410
  orig: 188, 410
  offset: 0, 0
  index: -1
LAMP_on
  rotate: false
  xy: 1297, 7
  size: 188, 410
  orig: 188, 410
  offset: 0, 0
  index: -1
